from flask import Flask
import json
import urllib2
import os

app = Flask(__name__)


@app.route('/')
def get_data():
    test_data = json.load(urllib2.urlopen(
        "http://" + str(os.environ["server_ip"]) + ":5000/"))
    vari = []
    for u in {test[0] for test in test_data.values()}:
        count = 0
        for test in test_data.values():
            if test[0] == u and test[1]:
                count += 1
            if count <= int(os.environ["t"]):
                vari.append(u)
            else:
                vari.remove(u)
    return json.dumps(list({test for test in vari}))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7000, debug=True)
