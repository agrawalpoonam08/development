from flask import Flask
import netifaces
import socket
import json


app = Flask(__name__)


@app.route('/')
def get_vectors():
    '''
    This function returns the vectors ipaddress
    '''
    result_list = [netifaces.ifaddresses(iface)[
        netifaces.AF_INET][0][
            'addr'
    ] for iface in netifaces.interfaces() if netifaces.AF_INET in netifaces.ifaddresses(iface)] # noqa
    result = {}
    print ('The value is %s', result)
    bool_value = False
    for i in result_list:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        res = sock.connect_ex((str(i), 25))
        bool_value = True if res is 0 else False
        result[i] = [socket.gethostname(), bool_value]
    print ('The value is %s', result, type(result))
    return json.dumps(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
