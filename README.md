Steps to run and test the code:

Add to enviruntment variables where client runs:

    'export t=1'
    'export server_ip=<your server_ip>''


Run the application Onserver:
    `'python server/src/get_interfaces.py'`
    
Run the application on Onclient:
    `'python client/src/service_running.py'`
    
Testing OnserverCode:
```
    'nosetests tests'
    'nosetests --with-coverage --cover-erase --cover-package=.'
```


Testing On clientCode:
```
    'nosetests tests'
    'nosetests --with-coverage --cover-erase --cover-package=.'
```

